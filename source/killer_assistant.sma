#define PLUGIN_NAME		"Killer Assistant"
#define PLUGIN_VERSION	"1.2"
#define PLUGIN_AUTHOR	"StockX"

#include <amxmodx>
#include <reapi>

new const settings_text[][] =
{
	"// Минимальный урон по противнику для зачёта помощи^n",
	"ka_minimum_damage ^"50^"^n^n",
	"// Количество добавляемых фрагов помощнику^n",
	"ka_added_frags ^"1^"^n^n",
	"// Количество добавляемых денег помощнику^n",
	"// Значение 0 или меньше отключает функцию^n",
	"ka_added_money ^"150^"^n^n",
	"// Информация в чате для всех?^n",
	"// 0 - для всех^n",
	"// 1 - только помощнику^n",
	"ka_send_notify ^"1^""
};

new const lang_text[][] = 
{
	"[ru]^n",
	"ASSISTANT_NOTIFY = !g[KA]: !t<killer_name> !gпомог убить !t<victim_name>!g [!t<damage> урон!g]. Награда: !t$<added_money>^n^n",
	"[en]^n",
	"ASSISTANT_NOTIFY = !g[KA]: !t<killer_name> !gпомог убить !t<victim_name>!g [!t<damage> урон!g]. Награда: !t$<added_money>"
};

enum RegCvarsList
{
	reg_ka_minimum_damage,
	reg_ka_added_frags,
	reg_ka_added_money,
	reg_ka_send_notify
};

enum CvarsList
{
	Float:ka_minimum_damage,
	Float:ka_added_frags,
	ka_added_money,
	bool:ka_send_notify
};

new cvar[CvarsList];
new reg_cvar[RegCvarsList];

new assistant[MAX_PLAYERS + 1];
new Float:damage[MAX_PLAYERS + 1][MAX_PLAYERS + 1];

new msg_score_info;

public plugin_init()
{
	register_plugin(PLUGIN_NAME, PLUGIN_VERSION, PLUGIN_AUTHOR);

	Create_Settings_File(false, true);
	register_dictionary("killer_assistant.txt");

	reg_cvar[reg_ka_minimum_damage] = register_cvar("ka_minimum_damage", "50");
	reg_cvar[reg_ka_added_frags] = register_cvar("ka_added_frags", "1");
	reg_cvar[reg_ka_added_money] = register_cvar("ka_added_money", "150");
	reg_cvar[reg_ka_send_notify] = register_cvar("ka_send_notify", "1");

	RegisterHookChain(RG_CBasePlayer_Killed, "CBasePlayer_Killed_Post", 1);
	RegisterHookChain(RG_CBasePlayer_TakeDamage, "CBasePlayer_TakeDamage_Post", 1);
	RegisterHookChain(RG_CSGameRules_RestartRound, "CSGameRules_RestartRound_Pre", 0);

	msg_score_info = get_user_msgid("ScoreInfo");
}

public plugin_cfg()
{
	Create_Settings_File(true, false);

	cvar[ka_minimum_damage] = get_pcvar_float(reg_cvar[reg_ka_minimum_damage]);
	cvar[ka_added_frags] = get_pcvar_float(reg_cvar[reg_ka_added_frags]);
	cvar[ka_added_money] = get_pcvar_num(reg_cvar[reg_ka_added_money]);
	cvar[ka_send_notify] = get_pcvar_bool(reg_cvar[reg_ka_send_notify]);
}

public client_disconnected(id)
	data_reset(id);

public CSGameRules_RestartRound_Pre()
{
	for(new id = 1; id <= MaxClients; id++)
	{
		if(is_user_connected(id))
			data_reset(id);
	}
}

public CBasePlayer_TakeDamage_Post(const victim, inflictor, attacker, Float:FlDamage)
{
	if(attacker == victim)
		return HC_CONTINUE;

	if((attacker <= 0 || attacker > MaxClients) || (victim <= 0 || victim > MaxClients))
		return HC_CONTINUE;

	if(get_member(attacker, m_iTeam) == get_member(victim, m_iTeam))
		return HC_CONTINUE;

	damage[attacker][victim] += FlDamage;

	if(damage[attacker][victim] >= Float:cvar[ka_minimum_damage])
		assistant[victim] = attacker;

	return HC_CONTINUE;
}

public CBasePlayer_Killed_Post(const victim, killer)
{
	if((victim == killer) || (victim <= 0 || victim > MaxClients))
		return HC_CONTINUE;

	if(killer == assistant[victim] || !is_user_connected(assistant[victim]))
	{
		data_reset(victim);
		data_reset(assistant[victim]);

		return HC_CONTINUE;
	}

	new killer_name[MAX_NAME_LENGTH], victim_name[MAX_NAME_LENGTH];
	get_user_name(killer, killer_name, charsmax(killer_name));
	get_user_name(victim, victim_name, charsmax(victim_name));

	if(cvar[ka_added_money] > 0)
		rg_add_account(assistant[victim], cvar[ka_added_money]);

	new lang_data[192], id = LANG_SERVER;
	LookupLangKey(lang_data, charsmax(lang_data), "ASSISTANT_NOTIFY", id);

	if(lang_data[0] != EOS)
	{
		replace_string(lang_data, charsmax(lang_data), "!n", "^1");
		replace_string(lang_data, charsmax(lang_data), "!t", "^3");
		replace_string(lang_data, charsmax(lang_data), "!g", "^4");

		replace_string(lang_data, charsmax(lang_data), "<killer_name>", killer_name);
		replace_string(lang_data, charsmax(lang_data), "<victim_name>", victim_name);
		replace_string(lang_data, charsmax(lang_data), "<added_money>", fmt("%d", cvar[ka_added_money]));
		replace_string(lang_data, charsmax(lang_data), "<damage>", fmt("%0.f", damage[assistant[victim]][victim]));

		client_print_color(cvar[ka_send_notify] ? assistant[victim] : 0, print_team_default, lang_data);
	}

	new Float:frags = get_entvar(assistant[victim], var_frags);
	frags += cvar[ka_added_frags];
	set_entvar(assistant[victim], var_frags, frags);

	message_begin(MSG_ALL, msg_score_info);
	write_byte(assistant[victim]);
	write_short(floatround(frags));
	write_short(get_member(assistant[victim], m_iDeaths));
	write_short(0);
	write_short(get_member(assistant[victim], m_iTeam));
	message_end();

	damage[assistant[victim]][victim] = 0.0;

	return HC_CONTINUE;
}

public data_reset(id)
{
	assistant[id] = 0;
	arrayset(_:damage[id], _:0.0, sizeof(damage[]));
}

stock Create_Settings_File(config = false, lang = false)
{
	new file_path[MAX_FMT_LENGTH];

	if(config && !lang)
	{
		new configs_dir[MAX_FMT_LENGTH];
		get_localinfo("amxx_configsdir", configs_dir, charsmax(configs_dir));
		formatex(file_path, charsmax(file_path), "%s/killer_assistant.cfg", configs_dir);
	}
	else if(lang && !config)
	{
		new data_dir[MAX_FMT_LENGTH];
		get_localinfo("amxx_datadir", data_dir, charsmax(data_dir));
		formatex(file_path, charsmax(file_path), "%s/lang/killer_assistant.txt", data_dir);
	}

	new file_pointer = fopen(file_path, "rt");
	if(!file_pointer)
	{
		fclose(file_pointer);

		file_pointer = fopen(file_path, "at");

		if(config)
		{
			for(new i; i < sizeof(settings_text); i++)
				fputs(file_pointer, settings_text[i]);
		}
		else
		{
			for(new i; i < sizeof(lang_text); i++)
				fputs(file_pointer, lang_text[i]);
		}

		fclose(file_pointer);

		server_print("[Killer Assistant]: Файл %s был создан автоматический. [%s]", config ? "настроек" : "словаря", file_path);
	}
	else fclose(file_pointer);
}